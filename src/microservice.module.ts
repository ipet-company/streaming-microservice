import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { getMongoURI } from './config/mongo';
import { StreamModule } from './stream/stream.module';

@Module({
  imports: [
    StreamModule,
    MongooseModule.forRoot(getMongoURI()),
  ],
  controllers: [],
  providers: [],
})
export class MicroserviceModule {}
