const mongoCredentials = {
  user: 'admin',
  password: 'admin',
  host: 'ipet-db-6yjzv.gcp.mongodb.net',
  database: 'streaming',
};

export function getMongoURI() {
  const { user, password, host, database } = mongoCredentials;
  const scheme = 'mongodb+srv';

  return `${scheme}://${user}:${password}@${host}/${database}`;
}
