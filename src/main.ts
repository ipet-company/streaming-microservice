import { NestFactory } from '@nestjs/core';
import { MicroserviceModule } from './microservice.module';
import { Logger } from './logger/logger';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

const logger = new Logger('Main');

const port = process.env.PORT || 8820;

async function bootstrap() {
  const app = await NestFactory.create(MicroserviceModule);

  const options = new DocumentBuilder()
    .setTitle('Ipet - Streaming Microservice')
    .setDescription('Streaming Microservice')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('api', app, document);

  app.listen(port, () => {
    logger.info(`Streaming Microservice is listening on  port ${port}`);
  });
}

bootstrap();
