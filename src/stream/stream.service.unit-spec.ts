import { expect } from 'chai';
import { mock, instance, when, anything } from 'ts-mockito';
import mockingoose from 'mockingoose';
import * as mongoose from 'mongoose';
import { StreamSchema } from './stream.schema';
import { Model, Document } from 'mongoose';
import { Stream } from './stream.interface';
import { HttpService } from '@nestjs/common';
import { StreamService } from './stream.service';
import { of } from 'rxjs';

describe('StreamService', () => {
  const mockedHttpService: HttpService = mock(HttpService);
  when(mockedHttpService.get(anything())).thenReturn(
    of({
      data: null,
      status: 200,
      statusText: '',
      headers: null,
      config: null,
    }),
  );
  const mockedModel: Model<Stream & Document> = mongoose.model(
    'stream',
    StreamSchema,
  );

  let httpService: HttpService;
  let streamService: StreamService;

  beforeAll(() => {
    httpService = instance(mockedHttpService);
    streamService = new StreamService(mockedModel, httpService);
  });

  describe('getStream(userId: string)', () => {
    it('should find stream info', async () => {
      const hardCodedStreamInfo: Stream = {
        userId: 'userTest',
        url: 'testUrl',
      };
      mockingoose(mockedModel).toReturn(hardCodedStreamInfo, 'findOne');
      const url = await streamService.getStream('userTest');
      expect(url).to.be.a('string');
      expect(url).to.equal('testUrl');
      mockingoose(mockedModel).reset();
    });

    it('should not find stream info', async () => {
      const nonExistent = await streamService.getStream('nonExistent');
      expect(nonExistent).to.be.null;
    });
  });

  describe('insertStreamInfoIotInfo(streamInfo: StreamInfo)', () => {
    it('should execute create', async () => {
      const result = await streamService.createStream({
        userId: 'userTest',
        url: 'testUrl',
      });

      expect(result).to.equal(true);
    });
  });

  describe('deleteStreamInfoIotInfo(streamInfo: StreamInfo)', () => {
    it('should execute delete', async () => {
      const hardCodedStreamInfo: Stream = {
        userId: 'userTest',
        url: 'testUrl',
      };
      mockingoose(mockedModel).toReturn(hardCodedStreamInfo, 'deleteOne');
      const result = await streamService.deleteStream({
        userId: 'userTest',
        url: 'testUrl',
      });
      mockingoose(mockedModel).reset();
      expect(result).to.equal(true);
    });
  });
});
