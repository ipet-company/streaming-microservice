export interface Stream {
  userId: string;
  url: string;
}