import { expect } from 'chai';
import { Test, TestingModule } from '@nestjs/testing';
import { StreamModule } from './stream.module';
import { MongooseModule } from '@nestjs/mongoose';
import { StreamController } from './stream.controller';
import { Stream } from './stream.interface';
import { Logger } from '../logger/logger';

describe('StreamController Integration', () => {
  const mongoCredentials = {
    user: 'admin',
    password: 'admin',
    host: 'ipet-db-6yjzv.gcp.mongodb.net',
    database: 'streaming-test',
  };
  const { user, password, host, database } = mongoCredentials;
  const scheme = 'mongodb+srv';
  const uriTest = `${scheme}://${user}:${password}@${host}/${database}`;

  let streamController: StreamController;

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [StreamModule, MongooseModule.forRoot(uriTest)],
    })
      .overrideProvider(Logger)
      .useValue({ info: () => null })
      .compile();

    streamController = moduleRef.get<StreamController>(StreamController);
  });

  describe('should insert, get, delete, get test', () => {
    it('should get test url inserted', async () => {
      const testStreamInfo: Stream = {
        userId: 'testUser',
        url: 'https://www.google.com/',
      };
      // insert
      await streamController.enableUserStream(testStreamInfo);
      //get
      const receivedUrl = await streamController.getUserPrivateStream(
        testStreamInfo.userId,
      );
      expect(receivedUrl).to.equal(testStreamInfo.url);
      // delete
      await streamController.disableUserStream(testStreamInfo);
      // get
      const received2Url = await streamController.getUserPrivateStream(
        testStreamInfo.userId,
      );
      expect(received2Url).to.be.null;
    });
  });

  describe('invalid url test', () => {
    it('insert invalid url and try to get it', async () => {
      const testStreamInfo: Stream = {
        userId: 'testUser2',
        url: 'invalidUrl',
      };
      // insert
      await streamController.enableUserStream(testStreamInfo);
      //get
      const receivedUrl = await streamController.getUserPrivateStream(
        testStreamInfo.userId,
      );
      expect(receivedUrl).to.be.null;
    });
  });
});
