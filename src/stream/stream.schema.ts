import * as mongoose from 'mongoose';
export const StreamSchema = new mongoose.Schema({
  userId: String,
  url: String
});
