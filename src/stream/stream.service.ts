import { Injectable, HttpService } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Document } from 'mongoose';
import { Stream } from './stream.interface';

@Injectable()
export class StreamService {
  constructor(
    @InjectModel('stream')
    private readonly streamModel: Model<Stream & Document>,
    private httpService: HttpService,
  ) {}

  async getStream(userId: string): Promise<string | null> {
    const stream = await this.streamModel.findOne({ userId: userId }).exec();

    if (!stream?.url) return null;

    if (!(await this.isStreamActive(stream.url))) {
      await this.deleteStream(stream);
      return null;
    }

    return stream?.url;
  }

  async createStream(streamInfo: Stream): Promise<boolean> {
    const result = await this.streamModel.create(streamInfo);
    return result !== null;
  }

  async deleteStream(streamInfo: Stream): Promise<boolean> {
    const result = await this.streamModel
      .deleteOne({ userId: streamInfo.userId, url: streamInfo.url })
      .exec();
    return result !== null;
  }

  private async isStreamActive(url: string): Promise<boolean> {
    try {
      return (await this.httpService.get(url).toPromise()).status == 200;
    } catch (error) {
      return false;
    }
  }
}
