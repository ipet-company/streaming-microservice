import {
  Controller,
  InternalServerErrorException,
  Get,
  Post,
  Body,
  Param,
} from '@nestjs/common';
import { StreamService } from './stream.service';
import { Stream } from './stream.interface';
import { Logger } from '../logger/logger';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('streaming')
@Controller('streaming')
export class StreamController {
  constructor(
    private readonly streamService: StreamService,
    private readonly logger: Logger,
  ) {}

  @Get('private-link/:id')
  async getUserPrivateStream(@Param('id') userId: string): Promise<string> {
    const logContext = `${StreamController.name}.${this.getUserPrivateStream.name}`;

    this.logger.info('A stream link was requested', logContext);

    try {
      const cool = await this.streamService.getStream(userId);
      console.log(cool);
      return cool;
    } catch (error) {
      this.logger.error(
        'It was not possible to return a stream link to the user',
        null,
        logContext,
        error,
      );

      throw new InternalServerErrorException(error.message);
    }
  }

  @Post('enable')
  async enableUserStream(@Body() stream: Stream) {
    this.logger.info('A request was made to enable a link stream', null, {
      stream,
    });

    await this.streamService.createStream(stream);
  }

  @Post('disable')
  async disableUserStream(@Body() stream: Stream) {
    this.logger.info('A request was made to disable a link stream', null, {
      stream,
    });

    await this.streamService.deleteStream(stream);
  }
}
