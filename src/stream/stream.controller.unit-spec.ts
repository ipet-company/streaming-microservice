import { expect } from 'chai';
import { mock, instance, when, verify, anything } from 'ts-mockito';
import { StreamService } from './stream.service';
import { StreamController } from './stream.controller';
import { Stream } from './stream.interface';
import { Logger } from '../logger/logger';

describe('StreamController', () => {
  const mockedLogger: Logger = mock(Logger);
  const mockedStreamService: StreamService = mock(StreamService);
  when(mockedStreamService.getStream('userTest')).thenResolve('testUrl');

  let streamService: StreamService;
  let logger: Logger;
  let streamController: StreamController;

  beforeAll(() => {
    logger = instance(mockedLogger);
    streamService = instance(mockedStreamService);

    streamController = new StreamController(streamService, logger);
  });

  describe('getUserPrivateStreamInfoIotInfo(userId: string)', () => {
    it('should log and return a string url', async () => {
      expect(await streamController.getUserPrivateStream('userTest')).to.equal(
        'testUrl',
      );
      verify(
        mockedLogger.info('A stream link was requested', anything()),
      ).once();
    });
  });

  describe('enableUserStreamInfoIotInfoFromIot(streamInfo: StreamInfo)', () => {
    it('should log and execute insert', async () => {
      const streamInfo: Stream = { userId: 'userTest', url: 'testUrl' };
      await streamController.enableUserStream(streamInfo);

      verify(
        mockedLogger.info(
          'A request was made to enable a link stream',
          anything(),
          anything(),
        ),
      ).once();
      verify(mockedStreamService.createStream(streamInfo)).once();
    });
  });

  describe('disableUserStreamInfoIotInfoFromIot(streamInfo: StreamInfo)', () => {
    it('should log and execute delete', async () => {
      const streamInfo: Stream = { userId: 'userTest', url: 'testUrl' };
      await streamController.disableUserStream(streamInfo);

      verify(
        mockedLogger.info(
          'A request was made to disable a link stream',
          anything(),
          anything(),
        ),
      ).once();

      verify(mockedStreamService.deleteStream(streamInfo)).once();
    });
  });
});
