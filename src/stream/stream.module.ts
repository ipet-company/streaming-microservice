import { Module, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StreamSchema } from './stream.schema';
import { StreamController } from './stream.controller';
import { StreamService } from './stream.service';
import { Logger } from '../logger/logger';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'stream', schema: StreamSchema }]),
    HttpModule,
  ],
  controllers: [StreamController],
  providers: [StreamService, Logger, String],
})
export class StreamModule {}
